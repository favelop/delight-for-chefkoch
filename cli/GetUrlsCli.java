import java.net.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.Arrays;
import java.util.ArrayList;


public class GetUrlsCli {

    private static final String TAG = "org.delight.chefkoch.geturls";
    private String html;

    public static void main(String args[])  //static method  
    {
        GetUrlsCli gu = new GetUrlsCli();
        try {
            if ("s".equals(args[0])) {
                gu.findRecipes(args[1]);
                
            }
            else if ("f".equals(args[0])) {
                gu.getFullRecipe(args[1]);
            }
        } catch (Exception e) {
        }
    }
    
    public ArrayList<String> findRecipes(String search) throws IOException  {
        String urlString = "https://www.chefkoch.de/rs/s0/" + search + "/Rezepte.html";
        System.out.println("Getting " + urlString);
        //Log.d(TAG, "Getting " + urlString);
    
        downloadHtml(urlString);
        return this.extractRecipes();
    }
    
    private ArrayList<String> extractRecipes() {
        ArrayList<String> arrayList = new ArrayList<String>();

        int current = 0;
        while (current != -1) {
            current = this.getNextRecipe(current, arrayList);
        }

        return arrayList;
    }
    
    
    private int getNextRecipe(int curr, ArrayList<String> arrayList) {
        curr = html.indexOf("class=\"ds-recipe-card bi-recipe-item\"", curr);
        
        if (curr == -1) return -1;
        
        curr = html.indexOf("data-vars-recipe-title=\"", curr) + 24;
        int titleStart = curr;
        curr = html.indexOf("\"", curr);
        int titleEnd = curr;
        
        
        // we need to search from first curr rating in curr1 (where the card begins
        //curr = html.indexOf("data-vars-rating=\"", curr) + 18;
        //int ratingStart = curr;
        //curr = html.indexOf("\"", curr);
        //int ratingEnd = curr;
        
        curr = html.indexOf("href=\"", curr) + 6;
        int startHref = curr;
        curr = html.indexOf("\"", curr);
        int endHref = curr;
        
        curr = html.indexOf("src=\"", curr) + 5;
        int startSrc = curr;
        curr = html.indexOf("\"", curr);
        int endSrc = curr;
        
        String href = html.substring(startHref, endHref);
        String title = html.substring(titleStart, titleEnd);
        //String rating = html.substring(ratingStart, ratingEnd);
        String img = html.substring(startSrc, endSrc);

        // add to list
        arrayList.add(href);
        
        System.out.println("title=" + title);
        //System.out.println("rating=" + rating);
        System.out.println("href=" + href);
        System.out.println("src=" + img);
        System.out.println("");
        //Log.d(TAG, "title=" + title + "\n" + "href=" + href + "\n" + "src=" + src + "\n\n");
        
        return curr;
    }
    
    public void getFullRecipe(String urlString) throws IOException  {
        //https://www.chefkoch.de/rezepte/1054911210586548/Schnelle-Lasagne.html
        
        if (!downloadHtml(urlString) || html == null) return;// new RecipeData("", "", "", new String[0]);
        
        int curr = 0;
        int off = html.indexOf("@type\": \"Recipe");
        
        // start from 0, because order of JSOn can be changed
        curr = html.indexOf("image\": \"", off) + 9;
        int startImg = curr;
        curr = html.indexOf("\"", curr);
        int endImg = curr;
        
        curr = html.indexOf("recipeIngredient\": [", off) + 20;
        int startIng = curr;
        curr = html.indexOf("]", curr);
        int endIng = curr;
        
        curr = html.indexOf("name\": \"", off) + 8;
        int startName = curr;
        curr = html.indexOf("\"", curr);
        int endName = curr;
        
        
        curr = html.indexOf("recipeInstructions\": \"", off) + 22;
        int startIns = curr;
        curr = html.indexOf("\"", curr);
        int endIns = curr;
        
        
        String image = html.substring(startImg, endImg);
        ArrayList<String> ingredients = convertJsonArray(html.substring(startIng, endIng));
        String name = html.substring(startName, endName);
        String instructions = html.substring(startIns, endIns);
        
        name = replaceUnicode(name);
        instructions = replaceUnicode(instructions);
        
        //Log.d(TAG, "name=" + name + "\n" + "image=" + image + "\n" + "ingredients=" + Arrays.toString(ia) + "\n" + "instructions=" + instructions + "\n");
        
        //return new RecipeData(name, instructions, image, ia);
        System.out.println("name=" + name);
        System.out.println("image=" + image);
        System.out.println("ingredients=" + String.join(", ", ingredients));
        System.out.println("instructions=" + instructions);
        System.out.println("");
    }
    
    private String replaceUnicode(String line) {
        String result = line;
        // Replace u characters
        Properties p = new Properties();
        try {
            p.load(new StringReader("key=" + line));
            result = p.getProperty( "key" );
        } catch ( IOException e ) { /*Log.e(TAG, e.toString());*/ }
        
        return result;
    }
    
    private ArrayList<String> convertJsonArray(String arrayString) {
        ArrayList<String> arrayList = new ArrayList<String>();

        int current = 0;
        while (current != -1) {
            current = this.getNextJsonArrayEntry(arrayString, current, arrayList);
        }

        return arrayList;
    }
    
    private int getNextJsonArrayEntry(String arrayString, int curr, ArrayList<String> arrayList) {
        if (curr >= arrayString.length()) return -1;
        curr = arrayString.indexOf("\"", curr);
        
        if (curr == -1) return -1;
        
        int entryStart = curr + 1;
        curr++; // no interest in "
        int entryEnd = arrayString.indexOf("\"", curr);
        curr = entryEnd + 1;
        curr++; // no interest in "
        
        String entry = arrayString.substring(entryStart, entryEnd);
        entry = replaceUnicode(entry);
        
        arrayList.add(entry);
        
        return curr;
    }
    
    private boolean downloadHtml(String urlString) throws IOException {
        // no catch should throw
        //try {
        URL url = new URL(urlString);
        URLConnection connection = url.openConnection();
        //connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/532.5 (KHTML, like Gecko) Chrome/4.0.249.0 Safari/532.5");
        // connection.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:89.0) Gecko/20100101 Firefox/89.0");
        connection.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        connection.setRequestProperty("DNT", "1");
        connection.setRequestProperty("Accept-language", "de-DE");
        
        
        connection.connect();

        int lenghtOfFile = connection.getContentLength();
        
        //System.out.println("Content-Length: " + lenghtOfFile);

        // download the file
        InputStream inputStream = new BufferedInputStream(url.openStream());
        
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            int nRead;
            byte[] data = new byte[1024];
            while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }
            //System.out.println("Read: " + nRead);

            buffer.flush();
            byte[] byteArray = buffer.toByteArray();
            inputStream.close();
            
            // done!
            html = new String(byteArray, StandardCharsets.UTF_8);
            
            return true;

        //} catch (Exception e) {
        //  Log.e(TAG, e.toString());
        //  throw e;
            
        //  return false;
        //}
    }
}
