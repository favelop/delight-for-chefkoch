ANDROIDSDK=/usr/lib/android-sdk/build-tools/debian
PLATFORM=/usr/lib/android-sdk/platforms/android-23/android.jar
MINSDK=19
APP=src/org/delight/chefkoch
CLASSPATH="src:lib/picasso-2.5.2.jar"
LIBS="lib/picasso-2.5.2.jar"

CLASSES=$(patsubst %.java,%.class,$(wildcard $(APP)/*.java))

# Resources:
# https://www.hanshq.net/command-line-android.html

apk/delight-for-chefkoch.apk: apk/delight-for-chefkoch.aligned.apk keystore.jks
	apksigner sign --ks keystore.jks --ks-key-alias androidkey --ks-pass pass:android --key-pass pass:android --out $@ $<

keystore.jks:
	keytool -genkeypair -keystore $@ -alias androidkey -validity 10000 -keyalg RSA -keysize 2048 -storepass android -keypass android

apk/delight-for-chefkoch.aligned.apk: apk/delight-for-chefkoch.unsigned.apk
	zipalign -f -p 4 $< $@

apk/delight-for-chefkoch.unsigned.apk: dex/classes.dex AndroidManifest.xml
	aapt package -f -v -F $@ -I $(PLATFORM) -M AndroidManifest.xml -S res dex

dex/classes.dex: $(CLASSES)
	[ -e dex ] || mkdir dex
	$(ANDROIDSDK)/dx --dex --verbose --min-sdk-version=$(MINSDK) --output=$@ src $(LIBS)

$(CLASSES): $(APP)/*.java $(APP)/R.java
	javac -cp $(CLASSPATH) -bootclasspath $(PLATFORM) -source 1.7 -target 1.7 $^

$(APP)/R.java: AndroidManifest.xml res/*
	aapt package -f -m -J src -S res -M AndroidManifest.xml -I $(PLATFORM)

clean:
	rm -vf	$(APP)/R.java \
		$(APP)/*.class \
		apk/*.unsigned.apk \
		apk/*.aligned.apk \
		dex/*.dex

distclean: clean
	[ ! -d dex ] || rmdir dex
	rm -vf *.apk

squeaky-clean: distclean
	@echo 'Warning! This will remove your signing keys!'
	@echo 'You have 5 seconds to press CTRL-C'
	@sleep 5
	rm -vf *.jks
