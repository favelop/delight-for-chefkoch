package org.delight.chefkoch;

class RecipeData {
   String Name;
   String Image;
   String Instructions;
   String[] Ingredients;

    public RecipeData(String name, String instructions, String image, String[] ingredients) {
        this.Name = name;
        this.Instructions = instructions;
        this.Image = image;
        this.Ingredients = ingredients;

    }
}
