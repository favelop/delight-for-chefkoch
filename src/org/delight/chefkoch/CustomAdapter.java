package org.delight.chefkoch;

import android.os.Bundle;
import android.content.Intent;
import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.net.*;

import com.squareup.picasso.Picasso;
import java.util.ArrayList;

class CustomAdapter implements ListAdapter {
   ArrayList<SubjectData> arrayList;
   Context context;
   public CustomAdapter(Context context, ArrayList<SubjectData> arrayList) {
      this.arrayList=arrayList;
      this.context=context;
   }
   
   @Override
   public boolean areAllItemsEnabled() {
      return false;
   }
   
   @Override
   public boolean isEnabled(int position) {
      return true;
   }
   
   @Override
   public void registerDataSetObserver(DataSetObserver observer) {
   }
   
   @Override
   public void unregisterDataSetObserver(DataSetObserver observer) {
   }
   
   @Override
   public int getCount() {
      return arrayList.size();
   }
   
   @Override
   public Object getItem(int position) {
      return position;
   }
   
   @Override
   public long getItemId(int position) {
      return position;
   }
   
   @Override
   public boolean hasStableIds() {
      return false;
   }
   
   @Override
   public View getView(int position, View convertView, ViewGroup parent) {
      final SubjectData subjectData = arrayList.get(position);
      if(convertView == null) {
         LayoutInflater layoutInflater = LayoutInflater.from(context);
         convertView = layoutInflater.inflate(R.layout.list_row, null);
         convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        Intent intent = new Intent(context, DetailActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(subjectData.Link));
        Bundle b = new Bundle();
        b.putString("url", subjectData.Link); //Your id
        intent.putExtras(b); //Put your id to your next Intent

        //startActivity(intent);
        v.getContext().startActivity(intent);
        //finish();

            }
         });
         TextView tittle = (TextView) convertView.findViewById(R.id.title);
         ImageView imag = (ImageView) convertView.findViewById(R.id.list_image);
         tittle.setText(subjectData.SubjectName);
         Picasso.with(context)
         .load(subjectData.Image)
         .into(imag);
      }
      return convertView;
   }
   
   @Override
   public int getItemViewType(int position) {
      return position;
   }
   
   @Override
   public int getViewTypeCount() {
      return arrayList.size();
   }
   
   @Override
   public boolean isEmpty() {
      return false;
   }
}
