package org.delight.chefkoch;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.ListView;
import android.widget.EditText;
import android.view.inputmethod.EditorInfo;
import android.view.KeyEvent;
import android.view.View;
import android.util.*;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends Activity {
    private static final String TAG = "org.delight.chefkoch.main";
    private ListView list;
    private EditText search;
    private WebScraper get;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list = (ListView) findViewById(R.id.list);

        get = new WebScraper();

        search = (EditText) findViewById(R.id.search);
        search.setOnKeyListener(new EditText.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
            // If the event is a key-down event on the "enter" button
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                (keyCode == KeyEvent.KEYCODE_ENTER)) {
              // Perform action on key press
              //Toast.makeText(MainActivity.this, search.getText(), Toast.LENGTH_SHORT).show();
                  // go async
                FindReceipesTask asyncTask = new FindReceipesTask();
                asyncTask.execute(search.getText().toString());

              return true;
            }
            return false;
            }
        });
   }

    protected void onSearch(String searchText) {
        //ArrayList<SubjectData> results = get.findRecipes(searchText);

        //CustomAdapter customAdapter = new CustomAdapter(this, results);
        //list.setAdapter(customAdapter);
    }
    
    private class FindReceipesTask extends AsyncTask<String, String, ArrayList<SubjectData>> {
    
        private ProgressDialog p;
        private Exception e;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            e = null;

            p = new ProgressDialog(MainActivity.this);
            p.setMessage("Searching...");
            p.setIndeterminate(false);
            p.setCancelable(false);
            p.show();
        }

        @Override
        protected ArrayList<SubjectData> doInBackground(String... searchText) {
            try {
                return get.findRecipes(searchText[0]);
            } catch (Exception e) {
                this.e = e;
                Log.e(TAG, e.toString());
                //Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
            }
            return new ArrayList<SubjectData>();
        }

        @Override
        protected void onPostExecute(ArrayList<SubjectData> results) {
            super.onPostExecute(results);
            p.hide();

            if (e != null) Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();

            try {
                CustomAdapter customAdapter = new CustomAdapter(getApplicationContext(), results);
                list.setAdapter(customAdapter);
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
        }
   }
}
