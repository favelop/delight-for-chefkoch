package org.delight.chefkoch;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.Toast;
import android.text.TextUtils;

import android.net.*;
import android.util.*;

import com.squareup.picasso.Picasso;

import java.util.Arrays;
import java.util.ArrayList;

public class DetailActivity extends Activity {

    private static final String TAG = "org.delight.chefkoch.detail";
    private String url;
    
    private TextView name;
    private TextView instructions;
    private ImageView image;
    private LinearLayout ingredients;
    private WebScraper get;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        url = ""; // or other values
        Intent intent = getIntent();
        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            Uri uri = intent.getData();
            url = uri.toString();
        } else {
            Bundle b = intent.getExtras();
            if(b != null)
                url = b.getString("url");
        }

        name = (TextView) findViewById(R.id.name);
        instructions = (TextView) findViewById(R.id.instructions);
        image = (ImageView) findViewById(R.id.image);
        ingredients = (LinearLayout) findViewById(R.id.ingredients);
        get = new WebScraper();

        // go async
        GetReceipeTask asyncTask = new GetReceipeTask();
        asyncTask.execute(url);
    }
   
    public void onClickBtn(View v)
    {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        v.getContext().startActivity(intent);
    }
    
    private class GetReceipeTask extends AsyncTask<String, String, RecipeData> {
    
        private ProgressDialog p;
        private Exception e;
        
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            e = null;

            p = new ProgressDialog(DetailActivity.this);
            p.setMessage("Downloading...");
            p.setIndeterminate(false);
            p.setCancelable(false);
            p.show();
        }

        @Override
        protected RecipeData doInBackground(String... url) {
            try {
                return get.getFullRecipe(url[0]);
            } catch (Exception e) {
                Log.e(TAG, e.toString());
                this.e = e;
            }
            return null;
        }

        @Override
        protected void onPostExecute(RecipeData rep) {
            super.onPostExecute(rep);
            p.hide();

            if (e != null) Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();

            try {
            Log.d(TAG, "name=" + rep.Name + "\n" + "image=" + rep.Image + "\n" + "ingredients=" + Arrays.toString(rep.Ingredients) + "\n" + "instructions=" + rep.Instructions + "\n");

            name.setText(rep.Name);

            // for text ingredients set this
            //String allInstr = TextUtils.join("\n", rep.Ingredients) + "\n\n" + rep.Instructions;
            instructions.setText(rep.Instructions); // and set allInstr here

            for (int i = 0; i < rep.Ingredients.length; i++) {
                CheckBox cb = new CheckBox(getApplicationContext());
                cb.setText(rep.Ingredients[i]);
                cb.setTextColor(0xFF000000);
                //cb.setScaleX(0.85f);
                //cb.setScaleY(0.85f);
                //LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, 
                //                   LayoutParams.WRAP_CONTENT);
                //cb.setLayoutParams(lp);
                cb.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                ingredients.addView(cb);
            }

            Picasso.with(getApplicationContext()).load(rep.Image).into(image);

            //ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, rep.Ingredients);
            //list.setAdapter(itemsAdapter);

            } catch (Exception e) {
            Log.e(TAG, e.toString());
            }
        }
    }
}
