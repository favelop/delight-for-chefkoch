package org.delight.chefkoch;

import android.util.*;

import java.net.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.Arrays;
import java.util.ArrayList;


public class GetUrls {

    private static final String TAG = "org.delight.chefkoch.geturls";
	private String html;

	public static void main(String args[])  //static method  
	{
		//GetUrls gu = new GetUrls();
		//if ("s".equals(args[0])) gu.findRecipes(args[1]);
		//else if ("f".equals(args[0])) gu.getFullRecipe(args[1]);
	}
	
	public ArrayList<SubjectData> findRecipes(String search) throws IOException  {
		String urlString = "https://www.chefkoch.de/rs/s0/" + search + "/Rezepte.html";
		//System.out.println("Getting " + urlString);
		Log.d(TAG, "Getting " + urlString);
	
		downloadHtml(urlString);
		return this.extractRecipes();
	}
	
	private ArrayList<SubjectData> extractRecipes() {
		ArrayList<SubjectData> arrayList = new ArrayList<SubjectData>();

		int current = 0;
		while (current != -1) {
			current = this.getNextRecipe(current, arrayList);
		}

		return arrayList;
	}
	
	
	private int getNextRecipe(int curr, ArrayList<SubjectData> arrayList) {
		curr = html.indexOf("<article", curr);
		
		if (curr == -1) return -1;
		
		curr = html.indexOf("href=\"", curr) + 6;
		int startHref = curr;
		curr = html.indexOf("\"", curr);
		int endHref = curr;
		
		curr = html.indexOf("data-vars-recipe-title=\"", curr) + 24;
		int titleStart = curr;
		curr = html.indexOf("\"", curr);
		int titleEnd = curr;
		
		curr = html.indexOf("src=\"", curr) + 5;
		int startSrc = curr;
		curr = html.indexOf("\"", curr);
		int endSrc = curr;
		
		String href = html.substring(startHref, endHref);
		String title = html.substring(titleStart, titleEnd);
		String src = html.substring(startSrc, endSrc);

		// add to list
		arrayList.add(new SubjectData(title, href, src));
		
		//System.out.println("title=" + title);
		//System.out.println("href=" + href);
		//System.out.println("src=" + src);
		//System.out.println("");
		Log.d(TAG, "title=" + title + "\n" + "href=" + href + "\n" + "src=" + src + "\n\n");
		
		return curr;
	}
	
	public RecipeData getFullRecipe(String urlString) throws IOException  {
		//https://www.chefkoch.de/rezepte/1054911210586548/Schnelle-Lasagne.html
		
		if (!downloadHtml(urlString) || html == null) return new RecipeData("", "", "", new String[0]);
		
		int curr = 0;
		int off = html.indexOf("@type\": \"Recipe");
		
		// start from 0, because order of JSOn can be changed
		curr = html.indexOf("image\": \"", off) + 9;
		int startImg = curr;
		curr = html.indexOf("\"", curr);
		int endImg = curr;
		
		curr = html.indexOf("recipeIngredient\": [", off) + 20;
		int startIng = curr;
		curr = html.indexOf("]", curr);
		int endIng = curr;
		
		curr = html.indexOf("name\": \"", off) + 8;
		int startName = curr;
		curr = html.indexOf("\"", curr);
		int endName = curr;
		
		
		curr = html.indexOf("recipeInstructions\": \"", off) + 22;
		int startIns = curr;
		curr = html.indexOf("\"", curr);
		int endIns = curr;
		
		
		String image = html.substring(startImg, endImg);
		String ingredients = html.substring(startIng, endIng);
		String name = html.substring(startName, endName);
		String instructions = html.substring(startIns, endIns);
		
		String[] ia = ingredients.split("\",");
		for (int i = 0; i < ia.length; i++) {
			int start = ia[i].indexOf("\"") + 1;
			ia[i] = ia[i].substring(start, ia[i].length());
			
			ia[i] = replaceUnicode(ia[i]);
		}
		
		name = replaceUnicode(name);
		instructions = replaceUnicode(instructions);
		
		Log.d(TAG, "name=" + name + "\n" + "image=" + image + "\n" + "ingredients=" + Arrays.toString(ia) + "\n" + "instructions=" + instructions + "\n");
		
		return new RecipeData(name, instructions, image, ia);
		//System.out.println("name=" + name);
		//System.out.println("image=" + image);
		//System.out.println("ingredients=" + Arrays.toString(ia));
		//System.out.println("instructions=" + instructions);
		//System.out.println("");
	}
	
	private String replaceUnicode(String line) {
		String result = line;
		// Replace u characters
		Properties p = new Properties();
		try {
			p.load(new StringReader("key=" + line));
			result = p.getProperty( "key" );
		} catch ( IOException e ) { Log.e(TAG, e.toString()); }
		
		return result;
	}
	
	private boolean downloadHtml(String urlString) throws IOException {
		// no catch should throw
		//try {
		        URL url = new URL(urlString);
		        URLConnection connection = url.openConnection();
		        //connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/532.5 (KHTML, like Gecko) Chrome/4.0.249.0 Safari/532.5");
		        // connection.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:89.0) Gecko/20100101 Firefox/89.0");
		        connection.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
		        connection.setRequestProperty("DNT", "1");
		        connection.setRequestProperty("Accept-language", "de-DE");
		        
		        
		        connection.connect();

		        int lenghtOfFile = connection.getContentLength();
		        
		        //System.out.println("Content-Length: " + lenghtOfFile);

		        // download the file
		        InputStream inputStream = new BufferedInputStream(url.openStream());
		        
		        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			int nRead;
			byte[] data = new byte[1024];
			while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
				buffer.write(data, 0, nRead);
			}
			//System.out.println("Read: " + nRead);

			buffer.flush();
			byte[] byteArray = buffer.toByteArray();
			inputStream.close();
		        
		        // done!
		        html = new String(byteArray, StandardCharsets.UTF_8);
		        
		        return true;

		//} catch (Exception e) {
		//	Log.e(TAG, e.toString());
		//	throw e;
			
		//	return false;
		//}
	}
}
