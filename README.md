# Delight for Chefkoch

A lightweight browser for the Chefkoch.de recipe website.

<img src="screenshot_list.png" alt="screenshot list" width=240px />
<img src="screenshot_recipe.png" alt="screenshot recipe" width=240px />

## Building

Install build dependencies (Debian 10):

    sudo apt install make android-sdk android-sdk-platform-23

Download project dependencies:

- Picasso 2.5.2 [Download](https://repo1.maven.org/maven2/com/squareup/picasso/picasso/2.5.2/picasso-2.5.2.jar)

Download the .jar file and place it in `lib/` folder.

To build just run `make`

On the first run, you will be asked for information to create your signing key.
If you're just trying things out, you can leave everything as-is.

Author of the example project file that builds with Debian build tools: [Coffee](https://gitlab.com/Matrixcoffee)